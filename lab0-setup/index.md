# Lab 0: Setup & Getting Started


Logging into all the Red Hat Products:

Let’s log into the Red Hat Products that you will use in this lab so they are ready to use.

In this lab application based self-signed SSL certs are used, please note that they are being used and should be accepted in order to complete the lab exercises.

Let’s log into the Red Hat Products that you will use in this lab so they are ready to use.

1. Open Firefox to The Red Hat Summit [GUID Grabber](https://www.opentlc.com/guidgrabber/guidgrabber.cgi) application in order to obtain your lab GUID.
2. For Lab Code select <LAB> - Red Hat Integrated Management Technologies Lab
3. Enter the activation key provided by the lab instructor then click Next. For this lab the key is <KEY> 
4. In your Red Hat Summit Lab Information webpage, take note of your assigned GUID. You will use this GUID to access your lab’s systems. Click on the link at the bottom of this page to access your lab environment.

![](images/image55.png) 

1.  From the lab environment information page, copy the hostname of the
    Workstation system (it should be workstation-GUID.rhpds.opentlc.com
    where GUID matches your environment’s GUID).
2.  Open a terminal window on your desktop environment and make sure you
    can SSH into the workstation host as you see below:
3.  [lab-user@localhost \~]\$ ssh
    [lab-user@workstation-GUID.rhpds.opentlc.com](mailto:lab-user@workstation-GUID.rhpds.opentlc.com)
4.  Run ‘sudo -i’ once you logon as lab-user to the jumpbox. This gives
    you root, and root has SSH keys for every host you will need to
    login to.

This step is not required, but If you need to troubleshoot or power
on/off/reboot a system, you can use the environment’s power control and
consoles by clicking the link on your GUID page. The password for any
consoles will be with username ‘root’ and password “r3dh4t1!”. From this
page, you will be able to access all of the Red Hat Products that you
will use in this lab. Press the start button at the top right to turn on
all the Red Hat Product VMs. Then, click on https for all the Red Hat
Products to access the UI. For applications, you may also log into the
UI of all the Red Hat Products with ‘admin’ as the Username and
“r3dh4t1!” (without the quotes) as the Password.

![](images/image40.png)

The following labs take place within the fictional EXAMPLE.COM company.


Continue to next step: [Lab 1: Red Hat Satellite for Content Lifecycle Management](../lab1-satellite-lifecycle/index.md)