# Lab 2: Satellite for Content Host Management

<!-- TOC -->

- [Lab 2: Satellite for Content Host Management](#lab-2-satellite-for-content-host-management)
	- [Create Activation Key:](#create-activation-key)
	- [Register Content Hosts](#register-content-hosts)
	- [Update Content Hosts:](#update-content-hosts)

<!-- /TOC -->

There are several preconfigured activation keys in the lab environment.
Some of these are required for subsequent lab functions. Do not remove these activation keys.

**Background:**

By creating an Activation Key, and associating the created Lifecycle Environments, Content Views, Subscriptions, and Repositories, we’ve enable the ability to activate simple, precise, and efficient deployment of new systems.

Activation Keys can be used to easily automate provisioning, as well as allow for consistent registration and initialization of Content Hosts without requiring users to obtain admin credentials.

### Create Activation Key:

Navigate to the Activation Keys page, select **Content** from the left hand navigation bar, and select **Activation Keys**

From the Activation Keys page, click the blue **Create Activation Key** button in the top right corner

Name the Activation Key **HOLKey**, and associate with your new Lifecycle Environment - **EXAMPLE_PROD** - and Content View - **Example.com** - from the previous lab

**NOTE:** We have pre-built a Lifecycle Environment and Content View in case you did not do Lab 1. If that’s the case, feel free to use these pre-built objects going forward.

 Click **Save** to save your Activation Key.

![](images/image28.jpg)

3.  Attach Subscriptions to Activation Key

	*  From the Activation Keys page, select your newly created activation key.
	*  Select **Subscriptions** from the Activation Key toolbar

	**NOTE:** If continuing from the previous step, this may direct you to this page automatically.

	* Select **Add** from the new section available.
	* Click the checkbox next to the **Red Hat Enterprise Linux Server, Premium (Physical or Virtual Nodes)** and then click **Add Selected** in the top right corner of the this new section.  

    **NOTE:** There may be multiple subscriptions of this type listed.  Select the first one.

![](images/image100.jpg)

4.  Configure **Enabled Repository** Default

	* From the Activation Keys page, select your newly created Activation Key
	* Select **Repository Sets** from the Activation Key toolbar

	**NOTE:** If continuing from the last step, this may direct you to this page automatically.

	**NOTE:** the repositories may take a moment to load due to using the Employee SKU in this example.

	* Click the checkbox next to each of the following repositories

	```
	Red Hat Enterprise Linux 7 Server (Kickstart)
	Red Hat Enterprise Linux 7 Server (RPMs)
    Red Hat Enterprise Linux 7 Server - Extras (RPMs)
	Red Hat Satellite Tools 6.5 (for RHEL 7 Server) (RPMs)
	```

	* Click **Select Action** followed by **Override to Enabled** in the top right corner of the Repository Selection section.

![](images/image7.jpg)

### Register Content Hosts

1.  Navigate to Content Hosts
	* To Navigate to the Content Hosts page, select **Hosts** from your toolbar, and select **Content Hosts** from the dropdown.

![](images/image41.jpg)

2.  Register Content Host

	* Click **Register Content Host** button in the top right corner
	* Log into host ic1.example.com and follow the steps outlined in the Satellite WebUI (See **Background** section for client access steps)

		* Select Content Source as **sat.example.com**
		* Install pre-built bootstrap RPM. This adjusts configurations on the host to point to your satellite/capsule for registration, subscription, and content delivery.
		* From the client console, run subscription-manager using the Activation Key created in Step 1. 
		* Install Katello Agent, which provides the ability to run remote execution (like content patching), as well as displays the errata status (applicable bugs, security, etc) for each Content Host.
		*   This step requires that the Activation key was properly configured to use the Satellite Tools repository
	
*For each host the commands should look like:

```
curl --insecure --output katello-ca-consumer-latest.noarch.rpm https://sat.example.com/pub/katello-ca-consumer-latest.noarch.rpm
yum localinstall katello-ca-consumer-latest.noarch.rpm 
subscription-manager register --org="Example_com" --activationkey="HOLKey"
yum -y install katello-agent
```

* Repeat for client systems ic2.example.com and ic3.example.com (See **Background** section on next page for client access steps). 
* ic4.example.com is already registered to the Satellite and you do not need to complete these steps.

![](images/image99.jpg)


### Update Content Hosts Errata:

**1.  Navigate to Content Host**

* To Navigate to the Content Hosts page, select **Hosts --> Content Hosts** from the dropdown.
* Notice after registering with the previously configured activation key, we now have the newly registered Content Host reporting it’s subscription status, Lifecycle Environment and Content View associations, and Installable Errata.
* Some of the hosts may have the .localdomain domain instead of .example.com domain. This is a limitation of the lab     environment’s networking, but all actions should complete successfully against these hosts in the environments.

![](images/image1.jpg)

**2.  Navigate to Content Host’s Installable Errata**

* From the Content Hosts page, select a Content Host, for example ic1.example.com

![](images/image96.jpg)

* This will bring up the Details page by default. From here, select **Errata** from the Content Hosts toolbar

![](images/image54.png)

**3.  Update Content Host Errata**

* Looking at the Errata for this system, it’s clear the system is severely out of date. Given this, we have a couple of options to update the clients (Specifically Push/Pull).
* **Push:** We can push specific errata from the satellite, to the client by selecting specific desired errata from this list
* Select the first 4 errata in the list
* Click **Apply Selected**
            
![](images/image78.png)
        
* Also note that you can select the drop down arrow next to **Apply Selected** and choose to apply the errata vie either the Katello agent or via remote execution.
* To Install All Errata you can select to view up to 100 available errata at a time at the bottom of the screen, and select all from the master checkbox in the top left corner of the view, then Apply Selected.
* Due to limited time in the lab you are only installing a few erratum - if you have time left over feel free to apply more.

![](images/image79.png)

**Note:** This Pull is an FYI and does not need to be performed in this lab environment this is for information purproses only
* Pull: Probably the most widely recognized method, and sometimes faster than selecting hundreds of errata from Satellite UI.
* Log into the client system via ssh, with a user with appropriate permissions
* Run the folowing command

	```
    [root@workstation-repl ~]\# ssh ic1.example.com
	[root@ic1 \~]\# yum -y update
	```
**Note:** this will update all packages without prompting, even packages that you may not want to be updated if you haven't done proper testing with your end applications.  In the lab environment this is fine to play with.

For systems registered to a prior Satellite, like this scenario: there may be some old remnants of the previous system registration. In this case, the remote push errata update may fail. You can login to the system and as root run the commands:
    ```
	# yum clean all ;  rm -rf /var/cache/yum/*
	```
* This clears out the old yum repo data from the previous satellite registration, and prepares the system for updates from your newly configured Satellite server.

Continue to the next step:[Lab 3: Proactive Security and Automated Risk Management with Red Hat Insights](../lab3-insights/index.md)
