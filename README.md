# Hands on Integrated Management Lab

The goal of these lab exercises are to introduce you to a variety of Red Hat products that can help you with managing and automating infrastructure resources. You will be able to demonstrate the power and flexibility of Red Hat management using either one or a combination of Red Hat products, such as Red Hat Satellite, Ansible Tower by Red Hat, Red Hat Insights, and Red Hat CloudForms.

Upon completion of this lab material, you should have a better understanding of how to perform  management tasks in your environments, how to more securely maintain hosts, and how to achieve greater efficiencies with automating tasks and managing systems within your infrastructure.

Most of the labs are exclusive of any other lab - i.e. If you want to only perform the CloudForms lab, then you can skip to the CloudForms lab. The timing of the lab is structured so an experienced system administrator should be able to complete the entire lab in allotted time following the documentation as written. If you would like to focus on an individual lab or specific portfolio product, please use the LAB INDEX to select the Lab you would like to begin on. Lab 0       should be viewed before attempting any other labs. If there is a problem with a step in your lab please raise your hand and contact one of the lab instructors.

This lab is geared towards systems administrators, cloud administrators and operators, architects, and others working on infrastructure operations interested in learning how to automate management across a heterogeneous infrastructure. The prerequisite for this lab include basic Linux skills gained from Red Hat Certified System Administrator (RHCSA) or equivalent system administration skills. Knowledge of virtualization and basic Linux scripting would also be helpful, but not required.

There are lab assistants and presenters roaming the room to help you with issues or answer questions - just raise your hand!

# Intro slides

You can find the intro slides [here](https://docs.google.com/presentation/d/1lrmTp3F2DgByPB0nRV5bEa3W1zgcoEzYNxlrmWNORTI/edit#slide=id.g5c7ba2e508_0_200)

## Table of Content

[Lab 0: Setup & Getting Started](lab0-setup/index.md)

[Lab 1: Red Hat Satellite for Content Lifecycle Management](lab1-satellite-lifecycle/index.md)

[Lab 2: Satellite for Content Host Management](lab2-satellite-content/index.md)

[Lab 3: Proactive Security and Automated Risk Management with Red Hat Insights](lab3-insights/index.md)

[Lab 4: Introduction to Ansible Tower](lab4-tower/index.md)

[Lab 5: Automatic Remediation with Red Hat Insights and Ansible Tower](lab5-insights-and-tower/index.md)

[Lab 6: Build a Service Catalog with CloudForms](lab6-self-service-portal-with-cloudforms/index.md)

[Lab 7: CloudForms Ansible Example](lab7-cloudforms-and-ansible/index.md)
